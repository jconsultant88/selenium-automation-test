
#### Introduction
This Maven project is aimed to learn selenium automation

#### Dependencies
This project uses the following dependencies
 - selenium
 - guice
 - cucumber
 - serenity for reporting
 - logback for logging

#### Run parallel execution
``
mvn clean verify -P parallel-execution
``

References

https://github.com/grasshopper7/cuke3-4.11-parameter/blob/master/cuke3-4.11-parameter/src/test/java/annonymous/Configurer.java

https://serenity-bdd.github.io/theserenitybook/latest/cucumber.html

https://github.com/andrejs-ps/automated-tests-in-java-with-fluent-interface-using-webdriver-selenium

https://github.com/serenity-bdd/serenity-cucumber-starter

http://logback.qos.ch/manual/configuration.html

https://cucumber.io/docs/guides/parallel-execution/

https://github.com/cucumber/cucumber-jvm/pull/1700

https://github.com/serenity-bdd/serenity-jbehave/issues/118

