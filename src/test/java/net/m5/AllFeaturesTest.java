package net.m5;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/main/resources/features",
        glue = "net.m5.bdd",
        plugin = {"json:target/cucumber.json", "html:target/site/cucumber-pretty"}
)
public class AllFeaturesTest {

}
