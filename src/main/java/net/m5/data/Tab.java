package net.m5.data;

public enum Tab {
    ALL("All"), COURSES("Courses"), BLOG("Blog"), RESOURCES("Resources"), AUTHORS("Authors");


    private String value;

    Tab(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }

    public static Tab fromValue(String text) {
        for (Tab tab : Tab.values()) {
            if (String.valueOf(tab.value).equals(text)) {
                return tab;
            }
        }
        return null;
    }
}