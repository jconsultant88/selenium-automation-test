package net.m5.data;

public enum SkillLevel {

    BEGINNER("Beginner"), INTERMEDIATE("Intermediate"), ADVANCED("Advanced");

    private String value;

    SkillLevel(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }

    public static SkillLevel fromValue(String text) {
        for (SkillLevel skillLevel : values()) {
            if (String.valueOf(skillLevel.value).equals(text)) {
                return skillLevel;
            }
        }
        return null;
    }

}
