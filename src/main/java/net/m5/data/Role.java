package net.m5.data;

public enum Role {

    BUSINESS_PROFESSIONAL("Business Professional"),
    SOFTWARE_DEVELOPMENT("Software Development");
    // and other

    private String value;

    Role(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }

    public static Role fromValue(String text) {
        for (Role role : Role.values()) {
            if (String.valueOf(role.value).equals(text)) {
                return role;
            }
        }
        return null;
    }
}
