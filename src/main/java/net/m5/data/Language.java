package net.m5.data;

public enum Language {
    JAVA("Java"),
    GO_LANG("Go lang"),
    C_SHARP("C Sharp");

    private String lang;

    Language(String lang) {
        this.lang = lang;
    }

    public String getLanguage() {
        return lang;
    }

    public String toString() {
        return lang;
    }

    public static Language fromValue(String text) {
        for (Language language : values()) {
            if (String.valueOf(language.lang).equals(text)) {
                return language;
            }
        }
        return null;
    }
}
