package net.m5.bdd;

import java.util.Locale;

import io.cucumber.core.api.TypeRegistry;
import io.cucumber.core.api.TypeRegistryConfigurer;
import io.cucumber.cucumberexpressions.ParameterType;
import io.cucumber.cucumberexpressions.Transformer;
import net.m5.data.Language;
import net.m5.data.Role;
import net.m5.data.SkillLevel;
import net.m5.data.Tab;

public class Configurer implements TypeRegistryConfigurer {

    @Override
    public void configureTypeRegistry(TypeRegistry registry) {

        registry.defineParameterType(
                new ParameterType<>("language", ".*?", Language.class, new Transformer<Language>() {
                    @Override
                    public Language transform(final String arg) throws Throwable {
                        return Language.valueOf(arg);
                    }
                }));

        registry.defineParameterType(
                new ParameterType<>("skillLevel", ".*?", SkillLevel.class, (String name) -> SkillLevel.fromValue(name)));

        registry.defineParameterType(
                new ParameterType<>("role", ".*?", Role.class, (String value) -> Role.fromValue(value)));

        registry.defineParameterType(
                new ParameterType<>("tab", ".*?", Tab.class, (String val) -> Tab.fromValue(val)));

    }

    @Override
    public Locale locale() {
        return Locale.ENGLISH;
    }
}