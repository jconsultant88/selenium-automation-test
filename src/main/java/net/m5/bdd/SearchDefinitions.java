package net.m5.bdd;

import com.google.inject.Inject;
import io.cucumber.guice.ScenarioScoped;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.extern.slf4j.Slf4j;
import net.m5.ApplicationProperties;
import net.m5.data.Language;
import net.m5.data.Role;
import net.m5.data.SkillLevel;
import net.m5.data.Tab;
import net.m5.pages.HomePage;
import net.m5.pages.SearchPage;
import org.openqa.selenium.WebDriver;

@Slf4j
@ScenarioScoped
public class SearchDefinitions {

    private String errorMessage;

    private WebDriver driver;

    @Inject
    private HomePage homePage;

    @Inject
    private SearchPage searchPage;

    @Inject
    public SearchDefinitions(ScenarioContainer scenarioContainer) {
        driver = scenarioContainer.webDriver;
    }

    @Given("User is in {string} page")
    public void userIsInHomePage(String page) {
        String homePageUrl = ApplicationProperties.getInstance().getBasePath() + ApplicationProperties.getInstance().getProperty("home.url");
        driver.get(homePageUrl);
        log.info("Navigate to: {} ", homePageUrl);
    }

    @When("User searches for {language}")
    public void userSearchesFor(Language language) {
        homePage.search(language.getLanguage());
    }

    @And("filters by skill {skillLevel}")
    public void filtersBySkillBeginner(SkillLevel skillLevel) {
        searchPage.filterBySkillLevel(skillLevel);
    }

    @And("role by {role}")
    public void roleBySoftwareDevelopment(Role role) {
        searchPage.filterByRole(role);
    }

    @And("selects tab {tab}")
    public void selectsTabCourses(Tab tab) {
        searchPage.selectTab(tab);
    }

    @And("select course {string}")
    public void selectCourse(String courseName) {
        searchPage.selectCourse(courseName);
    }

    @Then("{string} button is displayed")
    public void buttonIsDisplayed(String button) {
        System.out.println();
    }

}
