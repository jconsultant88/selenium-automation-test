package net.m5.bdd;

import io.cucumber.guice.ScenarioScoped;
import org.openqa.selenium.WebDriver;

@ScenarioScoped
public class ScenarioContainer {

    public WebDriver webDriver;
}
