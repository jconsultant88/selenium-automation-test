package net.m5.bdd;

import static net.m5.utils.DriverUtils.getChromeDriver;
import static net.m5.utils.DriverUtils.getChromeDriverPath;
import static net.m5.utils.DriverUtils.getFirefoxDriver;
import static net.m5.utils.DriverUtils.getFirefoxDriverPath;
import static net.m5.utils.DriverUtils.isDriverInstanceRunning;
import static net.m5.utils.MiscellaneousUtils.explicitWait;

import com.google.inject.Inject;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

public class CucumberApplicationHooks {

    static {
        System.setProperty("webdriver.chrome.driver", getChromeDriverPath());
        System.setProperty("webdriver.gecko.driver", getFirefoxDriverPath());
    }

    @Inject
    ScenarioContainer scenarioContainer;

    @Before(order = 10)
    public void beforeEveryScenario() {
        if (!isDriverInstanceRunning(scenarioContainer.webDriver)) {
            scenarioContainer.webDriver = getChromeDriver();
        }
    }

    @After
    public void afterEveryScenario(Scenario scenario) {

        //if (scenario.isFailed()) {
            // Take a screenshot...
            final byte[] screenshot = ((TakesScreenshot) scenarioContainer.webDriver)
                    .getScreenshotAs(OutputType.BYTES);
            // embed it in the report.
            scenario.attach(screenshot, "image/png", scenario.getName());
        //}
        explicitWait();
        scenarioContainer.webDriver.close();
    }

    @Before(order = 1, value = "@chrome")
    public void beforeChrome() throws Exception {
        if (!isDriverInstanceRunning(scenarioContainer.webDriver)) {
            scenarioContainer.webDriver = getChromeDriver();
        }
    }

    @Before(order = 1, value = "@firefox")
    public void beforeFirefox() throws Exception {
        if (!isDriverInstanceRunning(scenarioContainer.webDriver)) {
            scenarioContainer.webDriver = getFirefoxDriver();
        }
    }

}
