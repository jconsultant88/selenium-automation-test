package net.m5.pages;

import static net.m5.utils.MiscellaneousUtils.explicitWait;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOfAllElementsLocatedBy;

import com.google.inject.Inject;
import io.cucumber.guice.ScenarioScoped;
import lombok.extern.slf4j.Slf4j;
import net.m5.bdd.ScenarioContainer;
import net.m5.data.Role;
import net.m5.data.SkillLevel;
import net.m5.data.Tab;
import net.m5.utils.DriverUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

@Slf4j
@ScenarioScoped
public class SearchPage {

    private final WebDriver driver;
    private final WebDriverWait wait;

    @Inject
    public SearchPage(ScenarioContainer scenarioContainer) {
        driver = scenarioContainer.webDriver;
        wait = DriverUtils.getWebDriverWait(driver);
    }

    public SearchPage filterBySkillLevel(SkillLevel value) {
        log.info("Filtering by skill level {} ", value);

        driver.findElement(By.xpath(
                "//div[contains(@class, 'search-filter-header') and contains(.,'Skill Levels')]"))
                .click();

        By skillFilter = By
                .xpath("//span[contains(@class, 'search-filter-option-text') and contains(.,'"
                        + value + "')]");
        wait.until(visibilityOfAllElementsLocatedBy(skillFilter));
        driver.findElement(skillFilter)
                .click();

        explicitWait();
        return this;
    }

    public SearchPage filterByRole(Role role) {
        driver.findElement(
                By.xpath("//div[contains(@class, 'search-filter-header') and contains(.,'Roles')]"))
                .click();

        By roleFilter = By
                .xpath("//span[contains(@class, 'search-filter-option-text') and contains(.,'"
                        + role + "')]");
        wait.until(visibilityOfAllElementsLocatedBy(roleFilter));
        driver.findElement(roleFilter)
                .click();

        explicitWait();
        return this;
    }

    public SearchPage selectTab(Tab tab) {
        driver.findElement(By.xpath("//a[contains(@class, 'tab') and contains(., '" + tab + "')]"))
                .click();
        return this;
    }

    public void selectCourse(String courseName) {
        driver.findElement(By.xpath("//div[@id='search-results-category-target']" +
                "//div[@class='search-result columns' and contains(., '" + courseName + "')]" +
                "//div[@class='search-result__title']/a"))
                .click();
    }
}
