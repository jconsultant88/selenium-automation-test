package net.m5.pages;

import static net.m5.utils.DriverUtils.getChromeDriver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public abstract class AbstractPage {

    protected WebDriver driver = getChromeDriver();

    public AbstractPage verifyIsDisplayed(By element) {
        //Assert.assertTrue(driver.findElement(element).isDisplayed());
        return this;
    }

    public AbstractPage verifyIsNotDisplayed(By element) {
        // your code ...
        return this;
    }
}
