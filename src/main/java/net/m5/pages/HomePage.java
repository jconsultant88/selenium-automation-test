package net.m5.pages;

import com.google.inject.Inject;
import io.cucumber.guice.ScenarioScoped;
import net.m5.bdd.ScenarioContainer;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

@ScenarioScoped
public class HomePage {

    private final WebDriver driver;

    @Inject
    public HomePage(ScenarioContainer scenarioContainer) {
        driver = scenarioContainer.webDriver;
    }

    public void search(String value) {

        WebElement search = driver.findElement(By.className("header_search--input"));
        search.sendKeys(value);
        search.sendKeys(Keys.ENTER);
    }
}
