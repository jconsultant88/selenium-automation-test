package net.m5.utils;

import java.util.concurrent.TimeUnit;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

@Slf4j
public class DriverUtils {

    private static final String OS_NAME = System.getProperty("os.name").toLowerCase();
    private static final String CHROME_PATH = System.getProperty("user.dir") + "/chromedriver";
    private static final String FIREFOX_PATH = System.getProperty("user.dir") + "/geckodriver";

    private DriverUtils() {
        // prevent instantiation
    }

    public static WebDriver getChromeDriver() {
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().fullscreen();
        getWebDriverWait(driver);
        log.info("Created new instance of chrome logger");
        return driver;
    }

    public static WebDriver getFirefoxDriver() {
        WebDriver driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().fullscreen();
        getWebDriverWait(driver);
        return driver;
    }

    public static WebDriverWait getWebDriverWait(WebDriver webDriver) {
        return new WebDriverWait(webDriver, 5);
    }

    public static String getChromeDriverPath() {
        if (isWinOS()) {
            return CHROME_PATH + ".exe";
        }
        return CHROME_PATH;
    }

    public static String getFirefoxDriverPath() {
        if (isWinOS()) {
            return FIREFOX_PATH + ".exe";
        }
        return FIREFOX_PATH;
    }

    public static boolean isWinOS() {
        if (OS_NAME.indexOf("win") >= 0) {
            return true;
        }
        return false;
    }

    public static boolean isDriverInstanceRunning(WebDriver driver) {
        if (driver != null && driver.getCurrentUrl() != null) {
            return true;
        }
        return false;
    }
}
