package net.m5;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ApplicationProperties {
    private static ApplicationProperties instance;
    private final String HOME_PAGE_PATH = "file://" + System.getProperty("user.dir") + "/website/";
    private final static Object lock = new Object();

    private Properties properties;

    public static ApplicationProperties getInstance() {
        if (instance == null) {
            synchronized (lock) {
                instance = new ApplicationProperties();
            }
        }
        return instance;
    }

    public String getProperty(String key) {
        if (properties == null) {
            initialise();
        }
        return properties.getProperty(key);
    }

    public String getBasePath() {
        return HOME_PAGE_PATH;
    }

    private void initialise() {

        String fileName;
        if (System.getenv("test") != null) {
            fileName = "test.properties";
        } else if (System.getProperty("prod") != null) {
            fileName = "prod.properties";
        } else {
            fileName = "test.properties";
        }

        try {
            InputStream input = getClass()
                    .getClassLoader().getResourceAsStream(fileName);
            properties = new Properties();
            // load a properties file
            properties.load(input);
        } catch (IOException ex) {
            throw new RuntimeException(ex.getMessage());
        }
    }
}
