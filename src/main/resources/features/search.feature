Feature: Search for Java Fundamentals

  @chrome
  Scenario: Select 'Java Fundamentals'
    Given User is in 'home' page
    When User searches for JAVA
    And filters by skill Beginner
    And role by Software Development
    And selects tab Courses
    And select course 'Java Fundamentals: The Java Language'
    Then 'free trial' button is displayed

  @chrome
  Scenario: Select role 'Business Professional' and  course as 'Java Fundamentals'
    Given User is in 'home' page
    When User searches for JAVA
    And filters by skill Beginner
    And role by Business Professional
    And selects tab Courses
    And select course 'Java Fundamentals: The Java Language'
    Then 'free trial' button is displayed