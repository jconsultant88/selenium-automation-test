Feature: Search for Java EE: The Big Picture

  @chrome
  Scenario: Select skill as 'Beginner' and  'Java EE: The Big Picture'
    Given User is in 'home' page
    When User searches for JAVA
    And filters by skill Beginner
    And role by Software Development
    And selects tab Courses
    And select course 'Java EE: The Big Picture'
    Then 'free trial' button is displayed

  @chrome
  Scenario: Select skill as 'Intermediate' and  course as 'Java EE: The Big Picture'
    Given User is in 'home' page
    When User searches for JAVA
    And filters by skill Intermediate
    And role by Software Development
    And selects tab Courses
    And select course 'Java EE: The Big Picture'
    Then 'free trial' button is displayed